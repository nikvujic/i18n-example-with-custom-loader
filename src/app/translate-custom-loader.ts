/* eslint-disable eqeqeq */
/* eslint-disable @typescript-eslint/quotes */
import { TranslateLoader } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { readJSON } from './read-json.js';


export class TranslateCustomLoader implements TranslateLoader {
  getTranslation(lang: string): Observable<any> {
    if (lang == "fr"){
      const ru = readJSON('assets/i18n/fr.json');
      return of(ru);
    }
    if (lang == "es"){
      const ru = readJSON('assets/i18n/es.json');
      return of(ru);
    }
    const en = readJSON('assets/i18n/en.json');
    return of(en);
  }
}
